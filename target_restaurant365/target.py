"""Restaurant365 target class."""

from singer_sdk.target_base import Target
from singer_sdk import typing as th

from target_restaurant365.sinks import (
    BillsSink,
    JournalEntriesSink
)
from target_hotglue.target import TargetHotglue

class TargetRestaurant365(TargetHotglue):
    """Sample target for Restaurant365."""

    name = "target-restaurant365"
    SINK_TYPES = [BillsSink, JournalEntriesSink]
    config_jsonschema = th.PropertiesList(
        th.Property(
            "base_url",
            th.StringType,
            required=True
        ),
        th.Property(
            "username",
            th.StringType,
            required=True
        ),
        th.Property(
            "password",
            th.StringType,
        ),
    ).to_dict()

if __name__ == "__main__":
    TargetRestaurant365.cli()
