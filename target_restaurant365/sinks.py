"""Restaurant365 target sink class, which handles writing streams."""

from target_restaurant365.client import Restaurant365Sink

class BillsSink(Restaurant365Sink):
    """Restaurant365 target sink class."""
    endpoint = "/APInvoicesGL"
    name = "Bills"

    def prepare_payload(self, record):
        lines_arr = []
        payload = {
            "Type": "AP Invoice",
            "Vendor": record.get("vendorName"),
            "Date":  self.convert_datetime(record.get("issueDate")),
            "GL_Date":  self.convert_datetime(record.get("issueDate")),
            "Due_Date":  self.convert_datetime(record.get("dueDate")),
            "Location": record.get("locationId"),
            "Number": record.get("invoiceNumber"),
            "Amount": record.get("totalAmount"),
        }
        payload = self.clean_payload(payload)
        line_items = self.parse_json(record.get("lineItems", []))
        for line in line_items:
            line_map = {
                "Detail_Account": line.get("accountName"),
                "Detail_Amount": line.get("totalPrice"),
                "Detail_Comment": line.get("description"),
                "Detail_Location": line.get("locationId"),
            }
            line_map = self.clean_payload(line_map)
            line_map.update(payload)
            lines_arr.append(line_map)
        return lines_arr

    def preprocess_record(self, record: dict, context: dict) -> None:
        apInvoices = self.prepare_payload(record)
        mapping = {
            "BatchId": record.get("invoiceNumber"),
            "userId": self.config.get("username"),
            "apInvoices": apInvoices
        }
        return mapping
    
    def upsert_record(self, record: dict, context: dict):
        state_updates = dict()
        if record:
            buy_order_response = self.request_api(
                "POST", endpoint=self.endpoint, request_data=record
            )
            batch_id = record.get("BatchId")
            return batch_id, True, state_updates

class JournalEntriesSink(Restaurant365Sink):
    """Restaurant365 target sink class."""
    endpoint = "/JournalEntries"
    name = "JournalEntries"

    def prepare_payload(self, record):
        lines_arr = []
        line_items = self.parse_json(record.get("journalLines", record.get("lines")) or [])
        for line in line_items:
            line_map = {
                "JENumber": line.get("id"),
                "JELocation": line.get("locationId"),
                "Account": line.get("accountId", line.get("accountNumber")),
                "Debit": line.get("amount") if line.get("postingType") == "Debit" else None,
                "Credit": line.get("amount") if line.get("postingType") == "Credit" else None,
                "DetailLocation": line.get("locationId"),
                "Date": record.get("transactionDate")
            }
            line_map = self.clean_payload(line_map)
            lines_arr.append(line_map)
        return lines_arr

    def preprocess_record(self, record: dict, context: dict) -> None:
        journal_entries = self.prepare_payload(record)
        mapping = {
            "BatchId": record.get("id"),
            "userId": self.config.get("username"),
            "journalEntries": journal_entries
        }
        return mapping
    
    def upsert_record(self, record: dict, context: dict):
        state_updates = dict()
        if record:
            batch_id = record.get("BatchId")
            if not record.get("journalEntries"):
                self.update_state({"error": f"skipping batch with id {batch_id}, you have to send at least one journal line"})
            journal_entry_response = self.request_api(
                "POST", endpoint=self.endpoint, request_data=record
            )
            return batch_id, True, state_updates