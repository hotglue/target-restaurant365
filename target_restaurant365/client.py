from target_hotglue.client import HotglueSink
from datetime import datetime
import json
import ast
import requests
from singer_sdk.exceptions import FatalAPIError, RetriableAPIError

class Restaurant365Sink(HotglueSink):

    @property
    def base_url(self) -> str:
        base_url = self.config.get('base_url')
        base_url = base_url.rstrip("/")
        base_url = f"{base_url}/APIv1"
        return base_url
    
    @property
    def authenticator(self):
        access_token = self.config.get("access_token")
        if not access_token:
            request_data = {"UserName": self._config.get("username"), "Password": self._config.get("password")}
            token = requests.request("POST", f"{self.base_url}/Authenticate/JWT", params={"format": "json"}, data=request_data)
            if token.status_code == 200:
                access_token = token.json()["BearerToken"]
            else:
                raise Exception(token.text)
            #write in config file
            config_path = f"./config.json"
            with open(config_path) as f:
                config = json.load(f)
            config["access_token"] = access_token
            self._config["access_token"] = access_token
            with open(config_path, "w") as outfile:
                json.dump(config, outfile, indent=4)
        return f"{access_token}"

    @property
    def http_headers(self):
        auth_credentials = {
            "Authorization": self.authenticator
        }
        return auth_credentials

    def validate_input(self, record: dict):
        return self.unified_schema(**record).dict()

    def convert_datetime(self, date):
        if isinstance(date, datetime):
            return date.strftime("%Y-%m-%d")
        else:
            return date.split("T")[0]
    
    def parse_json(self, input):
        if isinstance(input, str):
            try:
                return ast.literal_eval(input)
            except:
                return json.loads(input)
        return input
    
    def validate_response(self, response: requests.Response) -> None:
        if response.status_code == 200:
            res_json = response.json()
            if res_json.get("errors", []) or res_json.get("failures", []):
                msg = {"errors": res_json.get("errors"), "failures": res_json.get("failures")}
                raise FatalAPIError(msg)
        if response.status_code in [429] or 500 <= response.status_code < 600:
            msg = self.response_error_message(response)
            raise RetriableAPIError(msg, response)
        elif 400 <= response.status_code < 500:
            try:
                msg = response.text
            except:
                msg = self.response_error_message(response)
            raise FatalAPIError(msg)
